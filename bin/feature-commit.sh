#!/usr/bin/env bash

UPDATED_COMMIT=$(git diff --name-only | grep commit.yml)

if [ "$UPDATED_COMMIT" = "commit.yml" ]; then
	echo "😁 Committing files"
  git add -A && git commit -F commit.yml
else
	echo "🙄 Please update commit.yml"
fi